# HttpLogger

## Objectif

Logger les appelles et les réponses d'une application spring mvc.

## Utilisation

```java
import fr.louarn.http.logger.filter.HttpLoggerFilter;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig extends HttpLoggerFilter {
}
```

## Exemple de log
 
[Méthode POST](src/test/resources/body.log)

Pour plus d'exemple cf le dossier `src/test/ressources`.
Pour modifier les templates cf `src/main/resources/template/logger` et la class `HttpTrace`.

## Sonar

[Le dashboard](https://sonarcloud.io/dashboard?id=com.gitlab.robin-ln%3Ahttp-logger)

## Gitlab-ci

- 1/ build
- 2/ test
- 3/ sonarqube
- 4/ checkstyle
- 5/ sonatype (publication)

## Sonatype

[Le ticket](https://issues.sonatype.org/browse/OSSRH-59103)

### Configuration

```groovy
maven {
    url "https://oss.sonatype.org/service/local/repositories/comgitlabrobin-ln-1000/content"
}
```

# TODO
- Utiliser des map et non des set
- rendre générique les piplines
- meilleur gestion des branches pour sonarqube
ex: 
En fonction de master ou d'une autre branches ajouter une option pour nommer les branches.
- utiliser un token pour sonatype
- résoudre les problèmes Deprecated Gradle features 