package com.gitlab.robin.ln.http.logger.modele;

import com.gitlab.robin.ln.http.logger.anonymizer.BodyAnonymizer;
import com.gitlab.robin.ln.http.logger.anonymizer.HeaderAnonymizer;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HttpLoggerAnonymizer {
    /**
     * headerAnonymizer.
     */
    private HeaderAnonymizer headerAnonymizer;

    /**
     * bodyAnonymizer.
     */
    private BodyAnonymizer bodyAnonymizer;
}
