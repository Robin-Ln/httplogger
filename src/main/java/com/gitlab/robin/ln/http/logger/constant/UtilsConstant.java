package com.gitlab.robin.ln.http.logger.constant;

public class UtilsConstant {

    /**
     * Taille maximale de la chaîne obfusquant la donnée (nombre d'étoiles *).
     */
    public static final int BODY_MAX_LENGTH_STRING = 50;

    /**
     * Taille maximale de la chaîne obfusquant la donnée (nombre d'étoiles *).
     */
    public static final int HEADER_MAX_LENGTH_STRING = 30;

    /**
     * Constructeur privé.
     */
    private UtilsConstant() {
    }
}
