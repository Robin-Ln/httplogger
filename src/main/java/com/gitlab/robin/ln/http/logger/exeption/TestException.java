package com.gitlab.robin.ln.http.logger.exeption;

public class TestException extends RuntimeException {

    /**
     * Constructeur avec paramètre.
     *
     * @param message message
     */
    public TestException(String message) {
        super(message);
    }
}
