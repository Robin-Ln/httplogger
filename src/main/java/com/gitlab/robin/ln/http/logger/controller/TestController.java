package com.gitlab.robin.ln.http.logger.controller;

import com.gitlab.robin.ln.http.logger.dto.TestDto;
import com.gitlab.robin.ln.http.logger.exeption.TestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "test")
public class TestController {

    /**
     * test get.
     *
     * @param id id
     * @return id
     */
    @GetMapping(value = "/{id}")
    public String get(@PathVariable Integer id) {
        return "test id : " + id;
    }

    /**
     * test post.
     *
     * @param testDto testDto
     * @return testDto
     */
    @PostMapping
    public TestDto post(@RequestBody TestDto testDto) {
        return testDto;
    }

    /**
     * test put error.
     *
     * @param testDto testDto
     */
    @PutMapping
    public ResponseEntity<TestDto> error(@RequestBody TestDto testDto) {
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(testDto);
        throw new TestException("Error: " + testDto.toString());
    }

}
