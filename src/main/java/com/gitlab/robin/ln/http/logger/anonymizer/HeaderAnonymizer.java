package com.gitlab.robin.ln.http.logger.anonymizer;

import com.gitlab.robin.ln.http.logger.constant.CaptureValueConstant;
import com.gitlab.robin.ln.http.logger.constant.UtilsConstant;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class HeaderAnonymizer implements ILoggerAnonymizer {

    /**
     * Liste des regex pour récupérer les valeurs des attributs souhaités.
     */
    private final List<Pattern> patterns = new ArrayList<>();

    /**
     * Liste des regex pour récupérer les valeurs des attributs souhaités.
     */
    private final Set<String> headers = new HashSet<>();

    /**
     * Ajouts de header a anonymiser.
     *
     * @param headers headers
     */
    @Override
    public void addFields(List<String> headers) {
        this.headers.addAll(headers);
        headers.forEach(header -> {
            String regexp = String.format(CaptureValueConstant.CAPTURING_HEADER_VALUE, header);
            patterns.add(Pattern.compile(regexp, Pattern.MULTILINE + Pattern.CASE_INSENSITIVE));
        });
    }

    /**
     * Permet d'anonimiser une ou plusieurs partie(s) d'une chaine de caractère.
     *
     * @param headers lites de tous les headers
     * @return une chaine de caractère anonymiser
     */
    public String anonymize(String headers) {

        if (headers == null) {
            return "";
        }

        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(headers);
            // Chaque header est unique, il ne peut être matché qu'une fois dans tous les headers
            if (matcher.find()) {
                headers = anonymiseData(headers, matcher, UtilsConstant.HEADER_MAX_LENGTH_STRING).toString();
            }
        }
        return headers;
    }

    /**
     * Permet d'anonimiser une ou plusieurs partie(s) d'une chaine de caractère.
     *
     * @param headers lites de tous les headers
     */
    public void anonymize(Map<String, String> headers) {
        if (headers != null) {
            this.headers.forEach(header ->
                    headers.entrySet().forEach(entry -> {
                        if (Objects.equals(header, entry.getKey())) {
                            entry.setValue(createAnonymeValue(
                                    entry.getValue(),
                                    UtilsConstant.HEADER_MAX_LENGTH_STRING)
                            );
                        }
                    })
            );
        }
    }

}
