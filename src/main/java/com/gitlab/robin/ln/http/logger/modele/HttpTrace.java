package com.gitlab.robin.ln.http.logger.modele;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.servlet.ServletException;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpTrace {

    /*
     * La requête
     */

    /**
     * requestMethod.
     */
    private String requestMethod;

    /**
     * requestURI.
     */
    private String requestURI;

    /**
     * requestURI.
     */
    private String requestParams;

    /**
     * requestProtocol.
     */
    private String requestProtocol;

    /**
     * requestHeaders.
     */
    private Map<String, String> requestHeaders;

    /**
     * requestBody.
     */
    private Body requestBody;

    /*
     * La réponse
     */

    /**
     * responseStatus.
     */
    private Integer responseStatus;

    /**
     * responseHeaders.
     */
    private Map<String, String> responseHeaders;

    /**
     * responseBody.
     */
    private Body responseBody;

    /**
     * exception
     */
    ServletException exception;

    /*
     * Temps d'éxécution
     */

    /**
     * startNs.
     */
    private Long startNs;

    /**
     * tookMs.
     */
    private Long tookMs;
}
