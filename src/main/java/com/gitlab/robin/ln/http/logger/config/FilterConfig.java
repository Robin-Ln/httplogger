package com.gitlab.robin.ln.http.logger.config;

import com.gitlab.robin.ln.http.logger.filter.HttpLoggerFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean<HttpLoggerFilter> loggingFilter() {

        // Création du filtre
        HttpLoggerFilter httpLoggerFilter = new HttpLoggerFilter();

        // Ajout du filtre et configuration
        final FilterRegistrationBean<HttpLoggerFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(httpLoggerFilter);
        registrationBean.addUrlPatterns("/*");

        return registrationBean;
    }
}
