package com.gitlab.robin.ln.http.logger.modele;

import com.gitlab.robin.ln.http.logger.exeption.FreeMarkerException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Getter
public enum Level {

    /**
     * NONE.
     */
    NONE("index-none.ftl"),

    /**
     * BASIC.
     */
    BASIC("index-basic.ftl"),

    /**
     * HEADERS.
     */
    HEADERS("index-headers.ftl"),

    /**
     * BODY.
     */
    BODY("index-body.ftl");

    /**
     * mustache.
     */
    private final Template template;

    /**
     * TEMPLATE_RESSOURCE.
     */
    private static final String TEMPLATE_RESSOURCE = "template/logger/%s";

    /**
     * RESSOURCE_PATH.
     */
    private static final String RESSOURCE_PATH = "src/main/resources";

    /**
     * Constructeur avec paramètre.
     *
     * @param templatePath template
     */
    Level(String templatePath) {
        try {
            Configuration configuration = createConfig();
            this.template = configuration.getTemplate(String.format(TEMPLATE_RESSOURCE, templatePath));
        } catch (IOException e) {
            throw new FreeMarkerException(e);
        }
    }

    private Configuration createConfig() throws IOException {
        Configuration configuration = new Configuration();
        configuration.setDirectoryForTemplateLoading(new File(RESSOURCE_PATH));
        configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return configuration;
    }
}
