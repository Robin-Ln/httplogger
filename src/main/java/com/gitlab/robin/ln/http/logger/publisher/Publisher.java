package com.gitlab.robin.ln.http.logger.publisher;

import com.gitlab.robin.ln.http.logger.anonymizer.BodyAnonymizer;
import com.gitlab.robin.ln.http.logger.anonymizer.HeaderAnonymizer;
import com.gitlab.robin.ln.http.logger.exeption.FreeMarkerException;
import com.gitlab.robin.ln.http.logger.filter.HttpLoggerFilter;
import com.gitlab.robin.ln.http.logger.modele.Body;
import com.gitlab.robin.ln.http.logger.modele.HttpLoggerAnonymizer;
import com.gitlab.robin.ln.http.logger.modele.HttpTrace;
import com.gitlab.robin.ln.http.logger.modele.Level;
import freemarker.template.TemplateException;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

public class Publisher {

    /**
     * Logger.
     */
    private final Consumer<String> log;

    /**
     * httpLoggerAnonymizer.
     */
    private final HttpLoggerAnonymizer httpLoggerAnonymizer;

    /**
     * DEFAULT_ANONYMIZER.
     */
    private static final HttpLoggerAnonymizer DEFAULT_ANONYMIZER = HttpLoggerAnonymizer
            .builder()
            .headerAnonymizer(new HeaderAnonymizer())
            .bodyAnonymizer(new BodyAnonymizer())
            .build();

    /**
     * Constructeur sans paramètres.
     */
    public Publisher() {
        this(
                LoggerFactory.getLogger(HttpLoggerFilter.class)::info,
                DEFAULT_ANONYMIZER
        );
    }

    /**
     * Constructeur avec paramètres.
     *
     * @param log log
     */
    public Publisher(Consumer<String> log) {
        this(log, DEFAULT_ANONYMIZER);
    }

    /**
     * Constructeur avec paramètres.
     *
     * @param log                  log
     * @param httpLoggerAnonymizer httpLoggerAnonymizer
     */
    public Publisher(Consumer<String> log, HttpLoggerAnonymizer httpLoggerAnonymizer) {
        this.log = log;
        this.httpLoggerAnonymizer = httpLoggerAnonymizer;
    }

    /**
     * Publication dans les log de la trace.
     *
     * @param httpTrace httpTrace
     * @param level     level
     */
    public void emit(HttpTrace httpTrace, Level level) {

        // anonymisation de la trace
        anonymise(httpTrace);

        // Création du scope
        StringWriter writer = new StringWriter();
        Map<String, Object> scope = new HashMap<>();
        scope.put("httpTrace", httpTrace);

        // Création de la trace
        try {
            level.getTemplate().process(scope, writer);
        } catch (TemplateException | IOException e) {
            throw new FreeMarkerException(e);
        }
        log.accept(writer.toString());
    }

    /**
     * Anonymisation de la trace.
     *
     * @param httpTrace httpTrace
     */
    private void anonymise(HttpTrace httpTrace) {
        // Récupération des content-type
        Optional<String> optionalRequestContentType = findContentType(httpTrace.getRequestHeaders());
        Optional<String> optionalResponseContentType = findContentType(httpTrace.getResponseHeaders());

        // les headers
        httpLoggerAnonymizer.getHeaderAnonymizer().anonymize(httpTrace.getRequestHeaders());
        httpLoggerAnonymizer.getHeaderAnonymizer().anonymize(httpTrace.getResponseHeaders());

        // les body
        anonymise(httpTrace.getResponseBody(), optionalRequestContentType);
        anonymise(httpTrace.getResponseBody(), optionalResponseContentType);
    }

    /**
     * Anonymise le body en fonction du content-type.
     *
     * @param body                body
     * @param optionalContentType optionalContentType.
     */
    private void anonymise(Body body, Optional<String> optionalContentType) {
        if (optionalContentType.isPresent()) {
            httpLoggerAnonymizer.getBodyAnonymizer().anonymize(body, optionalContentType.get());
        }
        httpLoggerAnonymizer.getBodyAnonymizer().anonymize(body);
    }

    /**
     * Récupération du content-type dans les headers.
     *
     * @param headers headers
     * @return content-type
     */
    private Optional<String> findContentType(Map<String, String> headers) {

        if (headers == null) {
            return Optional.empty();
        }

        return headers
                .entrySet()
                .stream()
                .filter(entry -> {
                    String key = entry.getKey().toLowerCase();
                    return Objects.equals("content-type", key);
                })
                .map(entry -> entry.getValue() == null ? "" : entry.getValue())
                .findFirst();
    }
}
