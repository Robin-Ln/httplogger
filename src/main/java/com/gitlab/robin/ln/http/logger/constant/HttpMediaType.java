package com.gitlab.robin.ln.http.logger.constant;

import lombok.Getter;
import org.springframework.http.MediaType;

import java.util.function.Function;
import java.util.regex.Pattern;

import static com.gitlab.robin.ln.http.logger.constant.CaptureValueConstant.CAPTURING_VALUE_JSON;
import static com.gitlab.robin.ln.http.logger.constant.CaptureValueConstant.CAPTURING_VALUE_X_FORM;

@Getter
public enum HttpMediaType {

    /**
     * Json.
     */
    JSON(MediaType.APPLICATION_JSON_VALUE, CAPTURING_VALUE_JSON),

    /**
     * Formulaire.
     */
    FORM(MediaType.APPLICATION_FORM_URLENCODED_VALUE, CAPTURING_VALUE_X_FORM);

    /**
     * Content type.
     */
    private final String contentType;

    /**
     * Pattern compiler.
     */
    private final Function<String, Pattern> patternCompiler;

    /**
     * Constructeur avec paramètre.
     *
     * @param contentType Type du body de la requête
     * @param regExp      expression régulière pour trouver la valeur sensible
     */
    HttpMediaType(String contentType, String regExp) {
        this.contentType = contentType;
        this.patternCompiler = field -> {
            String regularExpression = String.format(regExp, field);
            return Pattern.compile(regularExpression, Pattern.MULTILINE + Pattern.CASE_INSENSITIVE);
        };
    }
}
