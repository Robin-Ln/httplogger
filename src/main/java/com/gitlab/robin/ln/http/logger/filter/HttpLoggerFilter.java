package com.gitlab.robin.ln.http.logger.filter;


import com.gitlab.robin.ln.http.logger.modele.Body;
import com.gitlab.robin.ln.http.logger.modele.HttpTrace;
import com.gitlab.robin.ln.http.logger.modele.Level;
import com.gitlab.robin.ln.http.logger.publisher.Publisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HttpLoggerFilter extends OncePerRequestFilter {

    /*
     * Attributs
     */

    /**
     * Level.
     */
    private final Level level;

    /**
     * publisher.
     */
    private final Publisher publisher;

    /*
     * Constructeurs
     */

    /**
     * Constructeur sans paramètre.
     */
    public HttpLoggerFilter() {
        this(
                Level.BODY,
                new Publisher()
        );
    }

    /**
     * Constructeur avec paramètres.
     *
     * @param publisher publisher
     */
    public HttpLoggerFilter(Publisher publisher) {
        this(
                Level.BODY,
                publisher
        );
    }

    /**
     * Constructeur avec paramètres.
     *
     * @param level     level
     * @param publisher publisher
     */
    public HttpLoggerFilter(Level level, Publisher publisher) {
        this.level = level;
        this.publisher = publisher;
    }

    /*
     * le filtre
     */

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        // Initalisation des wrappers et du builder
        HttpTrace httpTrace = new HttpTrace();
        ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);


        try {
            // initialisation de chronomètrage de la requête
            httpTrace.setStartNs(System.nanoTime());
            filterChain.doFilter(requestWrapper, responseWrapper);
        } catch (ServletException e) {
            httpTrace.setException(e);
        } finally {
            afterFilter(httpTrace, requestWrapper, responseWrapper);
            responseWrapper.copyBodyToResponse();
            // Récupération des headers de la réponse
            httpTrace.setResponseHeaders(getResponseHeaders(responseWrapper));

            // Publication dans les log de la trace
            publisher.emit(httpTrace, level);

            if (httpTrace.getException() != null) {
                throw httpTrace.getException();
            }
        }
    }

    private void afterFilter(HttpTrace httpTrace, ContentCachingRequestWrapper request, ContentCachingResponseWrapper response) throws ServletException {
        // Calcule du temps d'éxécution de la requête
        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - httpTrace.getStartNs());
        httpTrace.setTookMs(tookMs);

        // Récupération des status
        httpTrace.setRequestMethod(request.getMethod());
        httpTrace.setRequestURI(request.getRequestURI());
        httpTrace.setRequestParams(request.getQueryString());
        httpTrace.setRequestProtocol(request.getProtocol());

        // Récupération des headers de la requête
        httpTrace.setRequestHeaders(getRequestHeaders(request));

        // Récupération du body de la requête
        httpTrace.setRequestBody(
                getContentBody(
                        request.getContentAsByteArray(),
                        request.getCharacterEncoding()
                )
        );

        ServletException exception = httpTrace.getException();
        boolean hasError = exception != null;

        // Récupération du status de la réponse
        httpTrace.setResponseStatus(hasError ? HttpStatus.INTERNAL_SERVER_ERROR.value() : response.getStatus());


        // Récupération du body de la response
        httpTrace.setResponseBody(
                hasError ?
                        Body
                                .builder()
                                .content(exception.getMessage())
                                .characterEncoding(response.getCharacterEncoding())
                                .build() :
                        getContentBody(
                                response.getContentAsByteArray(),
                                response.getCharacterEncoding()
                        )
        );
    }

    /*
     * les méthodes privées
     */

    /**
     * Récupération des headers de la requête.
     *
     * @param request requête
     * @return map
     */
    private Map<String, String> getRequestHeaders(ContentCachingRequestWrapper request) {
        Map<String, String> requestHeaders = new HashMap<>();

        Collections.list(request.getHeaderNames()).forEach(headerName -> {
            String headerValue = request.getHeaders(headerName).nextElement();
            requestHeaders.put(headerName, headerValue);
        });

        return requestHeaders;
    }

    /**
     * Récupération des headers de la response.
     *
     * @param response response
     * @return map
     */
    private Map<String, String> getResponseHeaders(ContentCachingResponseWrapper response) {
        Map<String, String> responseHeaders = new HashMap<>();

        response.getHeaderNames().forEach(headerName ->
                response.getHeaders(headerName).forEach(headerValue ->
                        responseHeaders.put(headerName, headerValue)
                )
        );

        return responseHeaders;
    }

    /**
     * Récupération du body.
     *
     * @param content         contenue
     * @param contentEncoding encodage du contenue
     * @return body
     */
    private Body getContentBody(byte[] content, String contentEncoding) {
        if (content.length > 0) {
            try {
                return Body
                        .builder()
                        .content(new String(content, contentEncoding))
                        .characterEncoding(contentEncoding)
                        .build();
            } catch (UnsupportedEncodingException e) {
                return Body
                        .builder()
                        .content(String.format("%s unsupported encoding content", contentEncoding))
                        .characterEncoding(contentEncoding)
                        .build();
            }
        }
        return Body
                .builder()
                .content("")
                .characterEncoding("")
                .build();
    }
}
