package com.gitlab.robin.ln.http.logger;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class AppApi extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(AppApi.class);
    }

    /**
     * Méthode principale de démarrage des services PFM.
     *
     * @param args {@link String}[] paramètres.
     */
    public static void main(String[] args) {
        SpringApplication.run(AppApi.class, args);
    }
}
