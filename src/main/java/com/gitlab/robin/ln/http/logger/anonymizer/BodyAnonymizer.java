package com.gitlab.robin.ln.http.logger.anonymizer;

import com.gitlab.robin.ln.http.logger.constant.HttpMediaType;
import com.gitlab.robin.ln.http.logger.modele.Body;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gitlab.robin.ln.http.logger.constant.UtilsConstant.BODY_MAX_LENGTH_STRING;

@Slf4j
public class BodyAnonymizer implements ILoggerAnonymizer {


    /**
     * Référentiel des éléments à anonymiser.
     */
    private final Map<String, Set<Pattern>> patternsMap;

    /**
     * Constructeur sans paramètre.
     */
    public BodyAnonymizer() {
        this.patternsMap = new HashMap<>();
    }

    @Override
    public void addFields(List<String> fields) {
        // Parcours des tous les champs à anonymiser
        fields.forEach(field -> {
            // Création d'un pattern pour tous les type de contenue possible
            for (HttpMediaType httpMediaType : HttpMediaType.values()) {
                putPattern(httpMediaType.getContentType(), httpMediaType.getPatternCompiler().apply(field));
            }
        });
    }

    /**
     * Permet d'anonimiser une ou plusieurs partie(s) d'une chaine de caractère.
     *
     * @param body contient la données avec des valeurs sensible
     */
    public void anonymize(Body body) {
        // On utilise par défaut du json
        anonymize(body, MediaType.APPLICATION_JSON_VALUE);
    }

    /**
     * Permet d'anonimiser une ou plusieurs partie(s) d'une chaine de caractère.
     *
     * @param body        contient la données avec des valeurs sensible
     * @param contentType contient le contentType, par défaut application/json
     */
    public void anonymize(Body body, String contentType) {

        if (body == null) {
            throw new NullPointerException("BodyAnonymizer::anonymize");
        }

        // Vérification de la donnée
        if (body.getContent() == null) {
            body.setContent("");
            body.setCharacterEncoding("");
            return;
        }

        // Vérification du contentType
        if (contentType == null) {
            return;
        }

        // Récupération des patterns en fonction du contenteType
        Set<Pattern> patterns = patternsMap.get(contentType);

        // Si pas de patterns
        if (patterns == null) {
            return;
        }

        // début de l'anonymisation du body
        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(body.getContent());
            // On boucle car on peut trouver plusieurs fois le même attribut dans le json
            while (matcher.find()) {
                String content = anonymiseData(body.getContent(), matcher, BODY_MAX_LENGTH_STRING).toString();
                body.setContent(content);
            }
        }
    }

    /**
     * Ajout d'un champ à anonymiser dans la liste du référentiel.
     *
     * @param contentType type du contenue du body
     * @param pattern     Pattern pour récupéré la valeur dans le body
     */
    private void putPattern(String contentType, Pattern pattern) {
        Set<Pattern> patterns = patternsMap.get(contentType);
        if (patterns == null) {
            patterns = new HashSet<>();
        }
        patterns.add(pattern);
        patternsMap.put(contentType, patterns);
    }

}
