package com.gitlab.robin.ln.http.logger.anonymizer;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;

public interface ILoggerAnonymizer {

    /**
     * Ajout des champs à anonymiser.
     *
     * @param fields liste de champs à anonymiser
     */
    void addFields(List<String> fields);

    /**
     * Remplacement de la valeur sensible par une valeur anonymiser.
     *
     * @param data      Chaine de caractères contenant une valeur sensible
     * @param matcher   Permet de localiser la valeur sensible dans un chaine de caractère
     * @param maxLength Taille max d'une valeur anonymiser
     * @return Retourne une chaine de caractères avec la donnée sensible anonymiser
     */
    default StringBuilder anonymiseData(String data, Matcher matcher, int maxLength) {
        // Récupération de la valeur
        String value = matcher.group(1);

        // Création de la valeur anonymiser
        String anonymeValue = createAnonymeValue(value, maxLength);

        // Récupération de la position de la valeur dans la data
        int startIndex = matcher.start(1);
        int endIndex = matcher.end(1);

        // remplacement de la valeur anonymiser dans la data
        return new StringBuilder()
                .append(data, 0, startIndex)
                .append(anonymeValue)
                .append(data, endIndex, data.length());
    }

    /**
     * Création d'une donnée anonymisé.
     *
     * @param value     value
     * @param maxLength maxLength
     * @return anonyme valeur
     */
    default String createAnonymeValue(String value, int maxLength) {
        return String.join(
                "",
                Collections.nCopies(value.length() % maxLength, "*")
        );
    }
}
