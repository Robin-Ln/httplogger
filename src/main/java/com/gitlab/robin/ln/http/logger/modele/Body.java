package com.gitlab.robin.ln.http.logger.modele;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Body {

    /**
     * content.
     */
    private String content;

    /**
     * characterEncoding.
     */
    private String characterEncoding;
}
