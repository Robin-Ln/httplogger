package com.gitlab.robin.ln.http.logger.constant;

public class CaptureValueConstant {

    /**
     * Regex capturant la valeur d'un attribut json.
     */
    public static final String CAPTURING_VALUE_JSON = "\"%s\":\\s*\"(.[^\\\"]*)\"";

    /**
     * Regex capturant la valeur d'un paramètre.
     */
    public static final String CAPTURING_VALUE_X_FORM = "%s=([^&]+)";

    /**
     * Regex capturant la valeur d'un header.
     */
    public static final String CAPTURING_HEADER_VALUE = "%s\\s*:\\s*(.[^\\n]*)";

    /**
     * Constructeur privé.
     */
    private CaptureValueConstant() {
    }
}
