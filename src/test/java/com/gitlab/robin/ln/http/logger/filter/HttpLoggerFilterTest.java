package com.gitlab.robin.ln.http.logger.filter;

import com.gitlab.robin.ln.http.logger.UnitTest;
import com.gitlab.robin.ln.http.logger.controller.TestController;
import com.gitlab.robin.ln.http.logger.modele.Body;
import com.gitlab.robin.ln.http.logger.modele.HttpTrace;
import com.gitlab.robin.ln.http.logger.publisher.Publisher;
import org.assertj.core.api.Assertions;
import org.easymock.Capture;
import org.easymock.EasyMock;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class HttpLoggerFilterTest extends UnitTest {

    /*
     * Les mocks
     */

    /**
     * publisher.
     */
    @Mock
    private Publisher publisher;

    /**
     * filter.
     */
    @TestSubject
    private HttpLoggerFilter filter;

    /**
     * mockMvc.
     */
    private MockMvc mockMvc;

    /*
     * Les données static
     */

    /**
     * BASE_PATH.
     */
    private static final String BASE_PATH = "/test";

    /**
     * GET_PATH.
     */
    private static final String GET_PATH = BASE_PATH + "/1";

    /**
     * PROTOCOL.
     */
    private static final String PROTOCOL = "HTTP/1.1";


    /**
     * JSON_CONTENT.
     */
    private static final String JSON_CONTENT = "{\"id\": \"1\"}";

    /**
     * JSON.
     */
    private static final Body JSON = Body
            .builder()
            .content(JSON_CONTENT)
            .characterEncoding("utf-8")
            .build();

    /**
     * GET_RESPONSE.
     */
    private static final Body GET_RESPONSE = Body
            .builder()
            .content("test id : 1")
            .characterEncoding("ISO-8859-1")
            .build();

    /**
     * PUT_ERROR_RESPONSE.
     */
    private static final Body PUT_ERROR_RESPONSE = Body
            .builder()
            .content("Error: TestDto(id=1)")
            .characterEncoding("")
            .build();

    /**
     * POST_HTTP_TRACE.
     */
    private static final HttpTrace POST_HTTP_TRACE = HttpTrace
            .builder()
            .requestBody(JSON)
            .requestMethod("POST")
            .requestProtocol(PROTOCOL)
            .requestURI(BASE_PATH)
            .responseBody(JSON)
            .responseStatus(HttpStatus.OK.value())
            .build();

    /**
     * GET_HTTP_TRACE.
     */
    private static final HttpTrace GET_HTTP_TRACE = HttpTrace
            .builder()
            .requestBody(
                    Body
                            .builder()
                            .content("")
                            .characterEncoding("")
                            .build()
            )
            .requestMethod("GET")
            .requestProtocol(PROTOCOL)
            .requestURI(GET_PATH)
            .responseBody(GET_RESPONSE)
            .responseStatus(HttpStatus.OK.value())
            .build();

    /**
     * PUT_ERROR_HTTP_TRACE.
     */
    private static final HttpTrace PUT_ERROR_HTTP_TRACE = HttpTrace
            .builder()
            .requestBody(JSON)
            .requestMethod("PUT")
            .requestProtocol(PROTOCOL)
            .requestURI(BASE_PATH)
            .responseBody(PUT_ERROR_RESPONSE)
            .responseStatus(HttpStatus.OK.value())
            .build();

    /*
     * Les tests
     */

    /**
     * init.
     */
    @Before
    public void init() {
        filter = new HttpLoggerFilter(publisher);
        mockMvc = standaloneSetup(new TestController())
                .addFilters(filter)
                .build();
    }

    /**
     * testPost.
     *
     * @throws Exception
     */
    @Test
    public void testPost() throws Exception {
        Capture<HttpTrace> httpTraceCapture = EasyMock.newCapture();
        publisher.emit(
                EasyMock.capture(httpTraceCapture),
                EasyMock.anyObject()
        );
        EasyMock.expectLastCall();

        replayAll();

        mockMvc.perform(
                post(BASE_PATH)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JSON_CONTENT)
        ).andReturn();

        checkHttpTrace(httpTraceCapture.getValue(), POST_HTTP_TRACE);
    }

    /**
     * testGet.
     *
     * @throws Exception
     */
    @Test
    public void testGet() throws Exception {
        Capture<HttpTrace> httpTraceCapture = EasyMock.newCapture();
        publisher.emit(
                EasyMock.capture(httpTraceCapture),
                EasyMock.anyObject()
        );
        EasyMock.expectLastCall();

        replayAll();

        mockMvc.perform(get(GET_PATH).characterEncoding("utf-8")).andReturn();

        checkHttpTrace(httpTraceCapture.getValue(), GET_HTTP_TRACE);
    }

    /**
     * testPutError.
     */
    @Test
    public void testPutError() {
        Capture<HttpTrace> httpTraceCapture = EasyMock.newCapture();
        publisher.emit(
                EasyMock.capture(httpTraceCapture),
                EasyMock.anyObject()
        );
        EasyMock.expectLastCall();

        replayAll();

        Assertions.assertThatThrownBy(() -> mockMvc.perform(
                put(BASE_PATH)
                        .characterEncoding("utf-8")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JSON_CONTENT)
                ).andReturn()
        ).isInstanceOf(NestedServletException.class);

        checkHttpTrace(httpTraceCapture.getValue(), PUT_ERROR_HTTP_TRACE);
    }

    /**
     * checkHttpTrace.
     *
     * @param capture   capture
     * @param expecting expecting
     */
    private void checkHttpTrace(HttpTrace capture, HttpTrace expecting) {
        Assertions.assertThat(capture)
                .extracting(
                        HttpTrace::getRequestMethod,
                        HttpTrace::getRequestURI,
                        HttpTrace::getRequestProtocol,
                        HttpTrace::getRequestBody,
                        HttpTrace::getResponseBody,
                        HttpTrace::getResponseStatus
                )
                .contains(
                        expecting.getRequestMethod(),
                        expecting.getRequestURI(),
                        expecting.getRequestProtocol(),
                        expecting.getRequestBody(),
                        expecting.getResponseBody(),
                        expecting.getResponseStatus()
                );
    }
}
