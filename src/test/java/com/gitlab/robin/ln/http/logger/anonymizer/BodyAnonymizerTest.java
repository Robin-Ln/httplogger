package com.gitlab.robin.ln.http.logger.anonymizer;

import com.gitlab.robin.ln.http.logger.UnitTest;
import com.gitlab.robin.ln.http.logger.constant.HttpMediaType;
import com.gitlab.robin.ln.http.logger.modele.Body;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class BodyAnonymizerTest extends UnitTest {

    /*
     * Données static
     */

    /**
     * FIELD_1.
     */
    private static final String FIELD_1 = "test";

    /**
     * FIELD_2.
     */
    private static final String FIELD_2 = "test2";

    /**
     * JSON_WITH_SPECIAL_CHARACTER.
     */
    private static final Body JSON_WITH_SPECIAL_CHARACTER = Body
            .builder()
            .content("{\"test\": \"*!*?*+$%.^*\"}")
            .content("utf-8")
            .build();

    /**
     * JSON.
     */
    private static final Body JSON = Body
            .builder()
            .content("{\"test\": \"value\", \"test2\": \"value\"}")
            .content("utf-8")
            .build();

    /**
     * JSON_WITH_EQUALS_KEYS.
     */
    private static final Body JSON_WITH_EQUALS_KEYS = Body
            .builder()
            .content("{\"test\": \"value\", \"test\": \"value\"}")
            .content("utf-8")
            .build();

    /**
     * X_FORM.
     */
    private static final Body X_FORM = Body
            .builder()
            .content("test=1234&test2=1234&test3=59")
            .content("utf-8")
            .build();

    /**
     * JSON_WITH_SPECIAL_CHARACTER_EXPECTED.
     */
    private static final Body JSON_WITH_SPECIAL_CHARACTER_EXPECTED = Body
            .builder()
            .content("{\"test\": \"***********\"}")
            .content("utf-8")
            .build();

    /**
     * EMPTY_BODY.
     */
    private static final Body EMPTY_BODY = Body.builder().build();

    /**
     * EMPTY_BODY.
     */
    private static final Body EMPTY_BODY_EXPECTED = Body
            .builder()
            .content("")
            .characterEncoding("")
            .build();

    /**
     * JSON_EXPECTED_1.
     */
    private static final Body JSON_EXPECTED_1 = Body
            .builder()
            .content("{\"test\": \"*****\", \"test2\": \"value\"}")
            .content("utf-8")
            .build();

    /**
     * JSON_EXPECTED_2.
     */
    private static final Body JSON_EXPECTED_2 = Body
            .builder()
            .content("{\"test\": \"value\", \"test2\": \"*****\"}")
            .content("utf-8")
            .build();

    /**
     * JSON_EXPECTED_3.
     */
    private static final Body JSON_EXPECTED_3 = Body
            .builder()
            .content("{\"test\": \"*****\", \"test\": \"*****\"}")
            .content("utf-8")
            .build();

    /**
     * X_FORM_EXPECTED_1.
     */
    private static final Body X_FORM_EXPECTED_1 = Body
            .builder()
            .content("test=****&test2=1234&test3=59")
            .content("utf-8")
            .build();

    /**
     * X_FORM_EXPECTED_2.
     */
    private static final Body X_FORM_EXPECTED_2 = Body
            .builder()
            .content("test=1234&test2=1234&test3=59")
            .content("utf-8")
            .build();

    /*
     * Les tests
     */

    /**
     * shouldAnonymizeSpecialCharacter.
     */
    @Test
    public void shouldAnonymizeSpecialCharacter() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList("test"));

        Body anonymize = JSON_WITH_SPECIAL_CHARACTER.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, HttpMediaType.JSON.getContentType());
        assertThat(anonymize).isEqualTo(JSON_WITH_SPECIAL_CHARACTER_EXPECTED);
    }

    /**
     * shouldAnonymizeWithEqualsValue.
     */
    @Test
    public void shouldAnonymizeWithEqualsValue() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList(FIELD_1));

        BodyAnonymizer bodyAnonymizer2 = new BodyAnonymizer();
        bodyAnonymizer2.addFields(Collections.singletonList(FIELD_2));

        Body anonymize = JSON.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, HttpMediaType.JSON.getContentType());
        assertThat(anonymize).isEqualTo(JSON_EXPECTED_1);

        Body anonymize2 = JSON.toBuilder().build();
        bodyAnonymizer2.anonymize(anonymize2, HttpMediaType.JSON.getContentType());
        assertThat(anonymize2).isEqualTo(JSON_EXPECTED_2);
    }

    /**
     * shouldAnonymizeWithEqualsKey.
     */
    @Test
    public void shouldAnonymizeWithEqualsKey() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList(FIELD_1));

        Body anonymize = JSON_WITH_EQUALS_KEYS.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, HttpMediaType.JSON.getContentType());
        assertThat(anonymize).isEqualTo(JSON_EXPECTED_3);
    }

    /**
     * shouldNotAnonymize.
     */
    @Test
    public void shouldNotAnonymize() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList(FIELD_1));

        Throwable thrown = catchThrowable(() -> bodyAnonymizer.anonymize(null));
        Assertions.assertThat(thrown).as("BodyAnonymizer::anonymize")
                .isInstanceOf(NullPointerException.class);
    }

    /**
     * shouldNotAnonymizeJson.
     */
    @Test
    public void shouldNotAnonymizeJson() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList("rien"));

        Body anonymize = JSON.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, HttpMediaType.JSON.getContentType());
        assertThat(anonymize).isEqualTo(JSON);
    }

    /**
     * shouldNotAnonymizeJson.
     */
    @Test
    public void shouldNotAnonymizeJsonWithNullContentType() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList("rien"));

        Body anonymize = JSON.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, null);
        assertThat(anonymize).isEqualTo(JSON);
    }

    /**
     * shouldAnonymizeForm.
     */
    @Test
    public void shouldAnonymizeForm() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList(FIELD_1));

        Body anonymize = X_FORM.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, HttpMediaType.FORM.getContentType());
        assertThat(anonymize).isEqualTo(X_FORM_EXPECTED_1);
    }

    /**
     * shouldNotAnonymizeForm.
     */
    @Test
    public void shouldNotAnonymizeForm() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList("rien"));

        Body anonymize = X_FORM.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, HttpMediaType.FORM.getContentType());
        assertThat(anonymize).isEqualTo(X_FORM_EXPECTED_2);
    }

    /**
     * shouldNotAnonymizeForm.
     */
    @Test
    public void shouldNotAnonymizeNullContent() {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(Collections.singletonList("rien"));

        Body anonymize = EMPTY_BODY.toBuilder().build();
        bodyAnonymizer.anonymize(anonymize, HttpMediaType.FORM.getContentType());
        assertThat(anonymize).isEqualTo(EMPTY_BODY_EXPECTED);
    }

}
