package com.gitlab.robin.ln.http.logger.anonymizer;

import com.gitlab.robin.ln.http.logger.UnitTest;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class HeaderAnonymizerTest extends UnitTest {

    /*
     * Les tests
     */

    /**
     * shouldAnonymize.
     */
    @Test
    public void shouldAnonymize() {
        String headers = "Host: myhost\nHeader: bonjour";
        String expectedHeaders = "Host: ******\n" + "Header: bonjour";
        HeaderAnonymizer headerAnonymizer = new HeaderAnonymizer();
        headerAnonymizer.addFields(Collections.singletonList("host"));

        String anonymize = headerAnonymizer.anonymize(headers);
        assertThat(anonymize).isEqualTo(expectedHeaders);
    }

    /**
     * shouldAnonymizeNullHeaders.
     */
    @Test
    public void shouldAnonymizeNullHeaders() {
        String headers = null;
        HeaderAnonymizer headerAnonymizer = new HeaderAnonymizer();
        headerAnonymizer.addFields(Collections.singletonList("host"));

        String anonymize = headerAnonymizer.anonymize(headers);
        assertThat(anonymize).isEmpty();
    }

    /**
     * shouldNotAnonymize.
     */
    @Test
    public void shouldNotAnonymize() {
        String expectedHeaders = "Host: myhost\nHeader: bonjour";
        HeaderAnonymizer headerAnonymizer = new HeaderAnonymizer();
        headerAnonymizer.addFields(Collections.singletonList("rien"));

        String anonymize = headerAnonymizer.anonymize(expectedHeaders);
        assertThat(anonymize).isEqualTo(expectedHeaders);
    }

    /**
     * shouldAnonymizeSet.
     */
    @Test
    public void shouldAnonymizeSet() {
        Map<String, String> headers = new HashMap<>();
        headers.put("key1", "value1");
        headers.put("key2", "value2");

        Map<String, String> expectedHeaders = new HashMap<>();
        expectedHeaders.put("key1", "value1");
        expectedHeaders.put("key2", "******");

        HeaderAnonymizer headerAnonymizer = new HeaderAnonymizer();
        headerAnonymizer.addFields(Collections.singletonList("key2"));

        headerAnonymizer.anonymize(headers);
        assertThat(headers.entrySet()).isEqualTo(expectedHeaders.entrySet());
    }

}
