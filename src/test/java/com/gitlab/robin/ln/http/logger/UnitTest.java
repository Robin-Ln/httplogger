package com.gitlab.robin.ln.http.logger;

import com.gitlab.robin.ln.http.logger.anonymizer.BodyAnonymizer;
import com.gitlab.robin.ln.http.logger.anonymizer.HeaderAnonymizer;
import com.gitlab.robin.ln.http.logger.modele.Body;
import com.gitlab.robin.ln.http.logger.modele.HttpTrace;
import org.easymock.EasyMockRule;
import org.easymock.EasyMockSupport;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(BlockJUnit4ClassRunner.class)
@Category({UnitTest.class})
public abstract class UnitTest extends EasyMockSupport {

    /**
     * easyMockRule.
     */
    @Rule
    public EasyMockRule easyMockRule = new EasyMockRule(this);

    /**
     * hasReplayed.
     */
    private boolean hasReplayed;

    /**
     * beforeMockTest.
     */
    @Before
    public void beforeMockTest() {
        if (this.hasReplayed) {
            this.hasReplayed = false;
            this.resetAll();
        }
    }

    /**
     * afterUnitTest.
     */
    @After
    public void afterUnitTest() {
        if (this.hasReplayed) {
            this.verifyAll();
        }
    }

    @Override
    public void replayAll() {
        this.hasReplayed = true;
        super.replayAll();
    }

    /*
     * Création de données
     */

    /**
     * Permet la récupération du contenue d'un fichier de ressource.
     *
     * @param ressourceFile ressourceFile
     * @return String
     * @throws IOException
     */
    public String resourceFileContent(String ressourceFile) throws IOException {
        URI uri = new ClassPathResource(ressourceFile).getURI();
        Path path = Paths.get(uri);
        return new String(Files.readAllBytes(path));
    }

    /**
     * headerAnonymizer.
     *
     * @param headers headers
     * @return HeaderAnonymizer
     */
    public HeaderAnonymizer headerAnonymizer(List<String> headers) {
        HeaderAnonymizer headerAnonymizer = new HeaderAnonymizer();
        headerAnonymizer.addFields(headers);
        return headerAnonymizer;
    }

    /**
     * bodyAnonymizer.
     *
     * @param fields fields
     * @return BodyAnonymizer
     */
    public BodyAnonymizer bodyAnonymizer(List<String> fields) {
        BodyAnonymizer bodyAnonymizer = new BodyAnonymizer();
        bodyAnonymizer.addFields(fields);
        return bodyAnonymizer;
    }

    /**
     * headers.
     *
     * @return Set<Map.Entry < String, String>>
     */
    public Map<String, String> headers() {
        Map<String, String> headers = new HashMap<>();
        headers.put("key1", "value1");
        headers.put("key2", "value2");
        headers.put("key3", "value3");
        return headers;
    }

    /**
     * headersWithContentType.
     *
     * @return Set<Map.Entry < String, String>>
     */
    public Map<String, String> headersWithContentType() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.put("key2", "value2");
        headers.put("key3", "value3");
        return headers;
    }

    /**
     * headersWithoutContentType.
     *
     * @return Set<Map.Entry < String, String>>
     */
    public Map<String, String> headersWithoutContentType() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-type", null);
        headers.put("key2", "value2");
        headers.put("key3", "value3");
        return headers;
    }

    /**
     * headersWithUnknownContentType.
     *
     * @return Set<Map.Entry < String, String>>
     */
    public Map<String, String> headersWithUnknownContentType() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-type", "unknown-Content-type");
        headers.put("key2", "value2");
        headers.put("key3", "value3");
        return headers;
    }

    /**
     * httpTrace.
     *
     * @param headers headers
     * @param body    body
     * @return HttpTrace
     */
    public HttpTrace httpTrace(Map<String, String> headers, Body body) {
        return HttpTrace
                .builder()
                .requestBody(body)
                .requestHeaders(headers)
                .requestMethod("requestMethod")
                .requestProtocol("requestProtocol")
                .requestURI("requestURI")
                .responseBody(body)
                .responseHeaders(headers)
                .responseStatus(HttpStatus.OK.value())
                .startNs(1L)
                .tookMs(1L)
                .build();
    }
}
