package com.gitlab.robin.ln.http.logger.publisher;


import com.gitlab.robin.ln.http.logger.UnitTest;
import com.gitlab.robin.ln.http.logger.modele.Body;
import com.gitlab.robin.ln.http.logger.modele.HttpLoggerAnonymizer;
import com.gitlab.robin.ln.http.logger.modele.Level;
import org.assertj.core.api.Assertions;
import org.easymock.Capture;
import org.easymock.EasyMock;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.catchThrowable;

public class PublisherTest extends UnitTest {

    /*
     * Donnée mocks
     */

    /**
     * logInfo.
     */
    @Mock
    private Consumer<String> logInfo;

    /**
     * publisher.
     */
    private Publisher publisher;

    /**
     * publisherWithAnonymizer.
     */
    private Publisher publisherWithAnonymizer;


    /*
     * Donnée static
     */

    /**
     * JSON_BODY.
     */
    private static final Body JSON_BODY = Body
            .builder()
            .content("{\"field1\":\"value1\", \"field2\":\"value2\", \"field3\":\"value3\"}")
            .characterEncoding("utf-8")
            .build();

    /**
     * X_FORM_BODY.
     */
    private static final Body X_FORM_BODY = Body
            .builder()
            .content("field1=value1&field2=value2&field3&value3")
            .characterEncoding("utf-8")
            .build();

    /**
     * BODY_LEVEL_LOG.
     */
    private static String bodyLevelLog;

    /**
     * HEADERS_LEVEL_LOG.
     */
    private static String headersLevelLog;

    /**
     * BASIC_LEVEL_LOG.
     */
    private static String basicLevelLog;

    /**
     * NULL_CONTENT_TYPE_ANONYMIZE_LOG.
     */
    private static String nullContentTypeAnonymizeLog;

    /**
     * HEADERS_JSON_BODY_ANONYMIZE_LOG.
     */
    private static String headersJsonBodyAnonymizeLog;

    /**
     * HEADERS_X_FORM_BODY_ANONYMIZE_LOG.
     */
    private static String headersXFormBodyAnonymizeLog;

    /**
     * UNKNOWN_CONTENT_TYPE_ANONYMIZE_LOG.
     */
    private static String unknownContentTypeAnonymizeLog;

    /**
     * WITHOUT_HEADERS_LOG.
     */
    private static String withoutHeadersLog;

    /**
     * WITHOUT_BODY_LOG.
     */
    private static String withoutBodyLog;

    /*
     * Les tests
     */

    /**
     * init.
     *
     * @throws IOException
     */
    @Before
    public void init() throws IOException {
        // ficher de ressource
        bodyLevelLog = resourceFileContent("body.log");
        headersLevelLog = resourceFileContent("headers.log");
        basicLevelLog = resourceFileContent("basic.log");
        nullContentTypeAnonymizeLog = resourceFileContent("anonymize-null-content-type.log");
        headersJsonBodyAnonymizeLog = resourceFileContent("anonymize-headers-json-body.log");
        headersXFormBodyAnonymizeLog = resourceFileContent("anonymize-headers-x-form-body.log");
        unknownContentTypeAnonymizeLog = resourceFileContent("anonymize-unknown-content-type.log");
        withoutHeadersLog = resourceFileContent("without-headers.log");
        withoutBodyLog = resourceFileContent("without-body.log");

        // anonymiser
        HttpLoggerAnonymizer httpLoggerAnonymizer = HttpLoggerAnonymizer
                .builder()
                .headerAnonymizer(headerAnonymizer(List.of("key1", "key2")))
                .bodyAnonymizer(bodyAnonymizer(List.of("field1", "field2")))
                .build();

        // Création des publishers
        publisher = new Publisher(logInfo);
        publisherWithAnonymizer = new Publisher(logInfo, httpLoggerAnonymizer);
    }

    /**
     * testBodyLevel.
     */
    @Test
    public void testBodyLevel() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisher.emit(httpTrace(headers(), JSON_BODY), Level.BODY);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(bodyLevelLog);
    }

    /**
     * testHeadersLevel.
     */
    @Test
    public void testHeadersLevel() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisher.emit(httpTrace(headers(), JSON_BODY), Level.HEADERS);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(headersLevelLog);
    }

    /**
     * testBasicLevel.
     */
    @Test
    public void testBasicLevel() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisher.emit(httpTrace(headers(), JSON_BODY), Level.BASIC);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(basicLevelLog);
    }

    /**
     * testNoneLevel.
     */
    @Test
    public void testNoneLevel() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisher.emit(httpTrace(headers(), JSON_BODY), Level.NONE);

        Assertions.assertThat(logCapture.getValue()).isEmpty();
    }


    /**
     * testHeadersAnonymiser.
     */
    @Test
    public void testHeadersAnonymiser() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisherWithAnonymizer.emit(httpTrace(headers(), JSON_BODY.toBuilder().build()), Level.BODY);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(headersJsonBodyAnonymizeLog);
    }

    /**
     * testJsonAnonymiser.
     */
    @Test
    public void testJsonAnonymiser() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisherWithAnonymizer.emit(httpTrace(headers(), JSON_BODY.toBuilder().build()), Level.BODY);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(headersJsonBodyAnonymizeLog);
    }

    /**
     * testJsonWithoutContentTypeAnonymiser.
     */
    @Test
    public void testJsonWithoutContentTypeAnonymiser() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisherWithAnonymizer.emit(httpTrace(headersWithoutContentType(), JSON_BODY.toBuilder().build()), Level.BODY);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(nullContentTypeAnonymizeLog);
    }

    /**
     * testJsonUnknownContentTypeAnonymiser.
     */
    @Test
    public void testJsonUnknownContentTypeAnonymiser() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisherWithAnonymizer.emit(
                httpTrace(
                        headersWithUnknownContentType(),
                        JSON_BODY.toBuilder().build()
                ),
                Level.BODY
        );

        Assertions.assertThat(logCapture.getValue()).isEqualTo(unknownContentTypeAnonymizeLog);
    }


    /**
     * testNullHeaders.
     */
    @Test
    public void testNullHeaders() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisher.emit(httpTrace(null, JSON_BODY.toBuilder().build()), Level.BODY);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(withoutHeadersLog);
    }


    /**
     * testNullBody.
     */
    @Test
    public void testNullBody() {
        replayAll();

        Throwable thrown = catchThrowable(() -> publisher.emit(httpTrace(headers(), null), Level.BODY));
        Assertions.assertThat(thrown).as("BodyAnonymizer::anonymize")
                .isInstanceOf(NullPointerException.class);
    }

    /**
     * testWithContentTypeXFormBody.
     */
    @Test
    public void testWithContentTypeXFormBody() {
        Capture<String> logCapture = EasyMock.newCapture();
        logInfo.accept(EasyMock.capture(logCapture));
        EasyMock.expectLastCall();

        replayAll();

        publisherWithAnonymizer.emit(httpTrace(headersWithContentType(), X_FORM_BODY.toBuilder().build()), Level.BODY);

        Assertions.assertThat(logCapture.getValue()).isEqualTo(headersXFormBodyAnonymizeLog);
    }
}
